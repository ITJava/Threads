package com.example.operator.console.question;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Question {
    private static Random rq = new Random();
    private static DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private String answerText;
    private String questionText;
    private static String[] questionsArray = new String[]{"What time is it now?",
            "How old are you?",
            "Есть мобила с камерой позвонить? Не боись, я свою симку вставлю, ёп-та!"};
    private long createdAt;
    private long answeredAt;

    public Question() {
        this.questionText = questionsArray[rq.nextInt(questionsArray.length-1)];
        this.createdAt = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return getFormattedCreatedAt() + " " + questionText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
        answeredAt = System.currentTimeMillis();
    }

    public String getFormattedCreatedAt(){
        return df.format(new Date(createdAt));
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getAnsweredAt() {
        return answeredAt;
    }

    public void setAnsweredAt(long answeredAt) {
        this.answeredAt = answeredAt;
    }
}
