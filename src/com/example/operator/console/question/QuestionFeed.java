package com.example.operator.console.question;

import java.util.Random;

public class QuestionFeed implements Runnable {
    private static QuestionFeed INSTANCE;
    Thread thread;
    QuestionListener listener;

    private QuestionFeed() {
        this.thread = new Thread(this, "questionFeedThread");
        this.thread.start();
    }

    @Override
    public void run() {
        Random r = new Random();
        while (true){
            try {
                Thread.sleep(10 + r.nextInt(9990));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Question q = new Question();
            if (listener != null){
                listener.process(q);
            }
        }
    }

    public void setListener(QuestionListener listener) {
        this.listener = listener;
    }

    public static QuestionFeed getInstance() {
        Class<QuestionFeed> cls = QuestionFeed.class;
        synchronized (QuestionFeed.class) {
            if (INSTANCE == null) {
                INSTANCE = new QuestionFeed();
            }
            return INSTANCE;
        }
    }

    public interface QuestionListener{
        void process(Question q);
    }
}
