package com.example.operator.console.main;

import com.example.operator.console.question.Question;
import com.example.operator.console.question.QuestionFeed;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedDeque;

public class Main implements QuestionFeed.QuestionListener {
    private static final Scanner sc = new Scanner(System.in);
    private Deque<Question> queue = new ConcurrentLinkedDeque<>();
    private List<Question> listAnsweredQuestion = new ArrayList<>();

    public Main() {
        QuestionFeed.getInstance().setListener(this);
        while (true) {
            if (queue.size() > 0) {
                Question q = queue.poll();
                System.out.println(q);
                q.setAnswerText(sc.nextLine());
                listAnsweredQuestion.add(q);
                System.out.println("List size : " + listAnsweredQuestion.size() + ", queue size " + queue.size());
            }
        }
    }

    @Override
    public void process(Question q) {
        queue.addLast(q);
    }

    public static void main(String[] args) {
        new Main();

        sc.close();
    }

}
